import axios from "axios";
import store from "../store";

// Client ID
// 7eaee019-25e4-4e49-8046-4aaa15bb8436
// Client Secret
// fdd22ff77d00dbbb318a3cb21611e94a9b14c1e
// window.btoa(unescape(encodeURIComponent("7eaee019-25e4-4e49-8046-4aaa15bb8436" + ":" + "fdd22ff77d00dbbb318a3cb21611e94a9b14c1e")));
// N2VhZWUwMTktMjVlNC00ZTQ5LTgwNDYtNGFhYTE1YmI4NDM2OmZkZDIyZmY3N2QwMGRiYmIzMThhM2NiMjE2MTFlOTRhOWIxNGMxZQ==
/* 
{
    "dataHeader": {
        "GW_RSLT_CD": "1200",
        "GW_RSLT_MSG": "오류 없음"
    },
    "dataBody": {
        "access_token": "6815ba41-33e8-41e6-9cf6-b47940ea8c02",
        "token_type": "bearer",
        "expires_in": 1576983049,
        "scope": "default"
    }
}
*/

const http = axios.create({
//    baseURL: "https://api.bizlivelab.com",
    baseURL: "http://localhost:8888",
  // baseURL: "http://bizlivelab.com:8888",
  // baseURL: "http://13.125.114.252:8888",
  headers: { "content-type": "application/json" },
});

http.interceptors.request.use(
  (config) => {
    const isAuthenticated = store.getters["isAuthenticated"];
    if (isAuthenticated) {
      const token = store.getters["getAccessToken"];
      config.headers.common["Authorization"] = `Bearer ${token}`;
      //console.log("http token", store.getters["getAccessToken"]);
    }
    return config;
  },
  (error) => {
    // Do something with request error
    Promise.reject(error);
  }
);
http.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

export default http;
